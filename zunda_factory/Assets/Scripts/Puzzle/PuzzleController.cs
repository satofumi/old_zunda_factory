﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleController : MonoBehaviour
{
    public Field Field;

    public Text ZpsValueText;


    private MoveGraph moveGraph;


    void Start()
    {
#if false
        Field.Size = new Vector2(7, 3);

        Field.SetPanel(Field.PanelName.Wall, new Vector2(0, 0));
        Field.SetPanel(Field.PanelName.Wall, new Vector2(6, 2));

        var zundaGate = Field.SetPanel(Field.PanelName.InputGate_Zunda, new Vector2(0, 2));
        var mochiGate = Field.SetPanel(Field.PanelName.InputGate_Mochi, new Vector2(0, 1));
        Field.SetPanel(Field.PanelName.OutputGate, new Vector2(6, 1));

        Field.SetPanel(Field.PanelName.Cooker, new Vector2(3, 1));

        Field.SetPanel(Field.PanelName.Conveyer_90, new Vector2(1, 2));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(1, 1));

        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(2, 1));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(4, 1));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(5, 1));
#endif

#if true
        Field.Size = new Vector2(12, 8);

        Field.SetPanel(Field.PanelName.InputGate_Kome, new Vector2(0, 1));
        Field.SetPanel(Field.PanelName.InputGate_Edamame, new Vector2(0, 5));

        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(1, 1));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(1, 5));

        Field.SetPanel(Field.PanelName.Cooker, new Vector2(2, 1));
        Field.SetPanel(Field.PanelName.Cooker, new Vector2(2, 5));

        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(3, 1));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(3, 5));

        Field.SetPanel(Field.PanelName.Cooker, new Vector2(4, 1));
        var zundaCooker = Field.SetPanel(Field.PanelName.Cooker, new Vector2(4, 5));

        Field.SetPanel(Field.PanelName.Conveyer_270, new Vector2(5, 1));
        Field.SetPanel(Field.PanelName.Conveyer_270, new Vector2(5, 2));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(5, 3));
        Field.SetPanel(Field.PanelName.Conveyer_90, new Vector2(5, 4));
        Field.SetPanel(Field.PanelName.Conveyer_90, new Vector2(5, 5));

        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(6, 3));

        Field.SetPanel(Field.PanelName.Cooker, new Vector2(7, 3));

        //Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(8, 3));
        Field.SetPanel(Field.PanelName.DoubleGate, new Vector2(8, 3));
        Field.SetPanel(Field.PanelName.Conveyer_270, new Vector2(8, 2));

        Field.SetPanel(Field.PanelName.Conveyer_90, new Vector2(9, 3));
        Field.SetPanel(Field.PanelName.Conveyer_90, new Vector2(9, 2));
        Field.SetPanel(Field.PanelName.Conveyer_180, new Vector2(9, 1));
        Field.SetPanel(Field.PanelName.Conveyer_180, new Vector2(8, 1));
        Field.SetPanel(Field.PanelName.Conveyer_270, new Vector2(7, 1));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(7, 2));

        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(9, 4));
        Field.SetPanel(Field.PanelName.Conveyer_90, new Vector2(10, 4));
        Field.SetPanel(Field.PanelName.Conveyer_0, new Vector2(10, 3));

        Field.SetPanel(Field.PanelName.OutputGate, new Vector2(11, 3));
#endif

        moveGraph = new MoveGraph();
        moveGraph.GenerateGraph(Field);

        //zundaCooker.GetComponent<Cooker>().DrawDebugPoints();

        Play();
    }

    void Update()
    {
        ZpsValueText.text = Field.Zps.ToString("F2") + " <size=24>zps</size>";
    }

    private void Play()
    {
        Field.IsPlaying = true;
    }
}
