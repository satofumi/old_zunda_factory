﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutputGate : MonoBehaviour
{
    private Material zundaMochi;
    private Machine machine;
    private List<float> releaseTicks = new List<float>();
    private float ticks;


    void Awake()
    {
        machine = GetComponent<Machine>();
    }

    void Update()
    {
        if (zundaMochi == null)
        {
            zundaMochi = machine.Materials.Prefab(Materials.Name.ZundaMochi);
            zundaMochi.transform.position = transform.position + Vector3.back;
        }

        ticks += Time.deltaTime;
        for (int i = 0; i < 2; ++i)
        {
            var material = machine.Materials.NodeMaterial(machine.Nodes[i]);
            if (material != null)
            {
                ReleaseMaterial(material);
            }
        }
    }

    private void ReleaseMaterial(Material material)
    {
        machine.Materials.Remove(material);
        material.Removed = true;

        releaseTicks.Add(ticks);
    }

    public void ResetCount()
    {
        releaseTicks.Clear();
        ticks = 0;
    }

    public List<float> ReleaseTimings()
    {
        return releaseTicks;
    }

    public void ClearReleaseTimings()
    {
        releaseTicks.Clear();
    }
}
