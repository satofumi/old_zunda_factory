﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
    public Materials Materials;

    public GameObject WallPrefab;
    public GameObject FloorPrefab;
    public GameObject InputGatePrefab;
    public GameObject OutputGatePrefab;
    public GameObject CookerPrefab;
    public GameObject DoubleGatePrefab;
    public GameObject WarpGatePrefab;
    public GameObject ConveyerPrefab;
    public GameObject PickerPrefab;


    public enum PanelName
    {
        Floor,
        Wall,

        InputGate_Edamame,
        InputGate_Yudemame,
        InputGate_Zunda,
        InputGate_Kome,
        InputGate_Rice,
        InputGate_Mochi,

        OutputGate,

        Cooker,

        DoubleGate,

        WarpGate_0,
        WarpGate_90,
        WarpGate_180,
        WarpGate_270,

        Conveyer_0,
        Conveyer_90,
        Conveyer_180,
        Conveyer_270,

        Picker_0,
        Picker_90,
        Picker_180,
        Picker_270,
    }

    private Dictionary<PanelName, GameObject> panelPrefabs;
    private List<GameObject> allPanels = new List<GameObject>();
    private Vector2 fieldSize;
    private Machine[,] gridMachines;
    private static Vector3 drawOffset;
    private List<OutputGate> outputGates = new List<OutputGate>();
    private List<float> releaseTicks = new List<float>();
    private float ticks;


    public GameObject Prefab(PanelName name)
    {
        if (panelPrefabs == null)
        {
            panelPrefabs = new Dictionary<PanelName, GameObject>()
            {
                { PanelName.Floor, FloorPrefab },
                { PanelName.Wall, WallPrefab },

                { PanelName.InputGate_Edamame, InputGatePrefab },
                { PanelName.InputGate_Yudemame, InputGatePrefab },
                { PanelName.InputGate_Zunda, InputGatePrefab },
                { PanelName.InputGate_Kome, InputGatePrefab },
                { PanelName.InputGate_Rice, InputGatePrefab },
                { PanelName.InputGate_Mochi, InputGatePrefab },

                { PanelName.OutputGate, OutputGatePrefab },

                { PanelName.Cooker, CookerPrefab },

                { PanelName.DoubleGate, DoubleGatePrefab },

                { PanelName.Conveyer_0, ConveyerPrefab },
                { PanelName.Conveyer_90, ConveyerPrefab },
                { PanelName.Conveyer_180, ConveyerPrefab },
                { PanelName.Conveyer_270, ConveyerPrefab },
            };
        }

        var panel = Instantiate(panelPrefabs[name]);
        var machine = panel.GetComponent<Machine>();
        if (machine != null)
        {
            machine.Name = name;
            machine.Materials = Materials;
        }


        switch (name)
        {
            case PanelName.InputGate_Edamame:
            case PanelName.InputGate_Yudemame:
            case PanelName.InputGate_Zunda:
            case PanelName.InputGate_Kome:
            case PanelName.InputGate_Rice:
            case PanelName.InputGate_Mochi:
                panel.GetComponent<InputGate>().SetInputMaterial(name);
                break;

            case PanelName.Conveyer_0:
            case PanelName.Conveyer_90:
            case PanelName.Conveyer_180:
            case PanelName.Conveyer_270:
                panel.GetComponent<Conveyer>().Direction = PanelDirection(name);
                break;

            case PanelName.OutputGate:
                outputGates.Add(panel.GetComponent<OutputGate>());
                break;
        }

        return panel;
    }

    void Start()
    {
    }

    void Update()
    {
        if (IsPlaying)
        {
            Materials.MoveAllMaterials();
            foreach (var gate in outputGates)
            {
                releaseTicks.AddRange(gate.ReleaseTimings());
                gate.ClearReleaseTimings();
            }

            ticks += Time.deltaTime;
            UpdateZps();
        }
        else
        {
            foreach (var gate in outputGates)
            {
                gate.ResetCount();
                ticks = 0;
            }
        }
    }

    public bool IsPlaying { set; get; }

    private void UpdateZps()
    {
        releaseTicks.RemoveAll(x => x < (ticks - Constant.ZpsCalculatePerios));
        if (releaseTicks.Count <= 1)
        {
            return;
        }

        int total = releaseTicks.Count;
        float period = releaseTicks[releaseTicks.Count - 1] - releaseTicks[0];
        Zps = total / period;
    }

    public float Zps { private set; get; }

    public static Vector3 DrawPosition(Vector2 grid)
    {
        return new Vector3(grid.x, -grid.y, 0) + drawOffset;
    }

    public Vector2 Size
    {
        set
        {
            ClearPanels();

            fieldSize = value;
            int width = (int)fieldSize.x;
            int height = (int)fieldSize.y;

            gridMachines = new Machine[width, height];

            drawOffset = new Vector3(-(width - 1) / 2, -((height - 1) / 2) + (height - 1) - 1, 0);
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    var panel = Prefab(PanelName.Floor);
                    panel.transform.position = new Vector3(x, -y, +1) + drawOffset;
                }
            }
        }
        get
        {
            return fieldSize;
        }
    }

    public Machine Machine(Vector2 grid)
    {
        return gridMachines[(int)grid.x, (int)grid.y];
    }

    public void ClearPanels()
    {
        foreach (var panel in allPanels)
        {
            Destroy(panel);
        }
        allPanels.Clear();
    }

    public GameObject SetPanel(PanelName name, Vector2 grid)
    {
        var panel = Prefab(name);
        panel.transform.position = new Vector3(grid.x, -grid.y, 0) + drawOffset;

        gridMachines[(int)grid.x, (int)grid.y] = panel.GetComponent<Machine>();

        return panel;
    }

    public int PanelDirection(PanelName name)
    {
        switch (name)
        {
            case PanelName.Conveyer_0:
            case PanelName.WarpGate_0:
            case PanelName.Picker_0:
                return 0;

            case PanelName.Conveyer_90:
            case PanelName.WarpGate_90:
            case PanelName.Picker_90:
                return 90;

            case PanelName.Conveyer_180:
            case PanelName.WarpGate_180:
            case PanelName.Picker_180:
                return 180;

            case PanelName.Conveyer_270:
            case PanelName.WarpGate_270:
            case PanelName.Picker_270:
                return 270;
        }

        // Invalid value
        return 45;
    }

    public void LoadStage(string stageName)
    {
        // !!!
    }
}
