﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveGraph
{
    public class Node
    {
        public Vector2 Position { set; get; }
        public Node NextNode { set; get; }

        public Node(Vector2 position, Node nextNode)
        {
            Position = position;
            NextNode = nextNode;
        }
    }


    private const int LineNodesPerGrid = 5;

    private Dictionary<Vector2, Node> nodes = new Dictionary<Vector2, Node>();


    public void GenerateGraph(Field field)
    {
        nodes.Clear();

        int width = (int)field.Size.x;
        int height = (int)field.Size.y;

        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                var grid = new Vector2(x, y);
                var machine = field.Machine(grid);
                if (machine == null)
                {
                    continue;
                }

                switch (machine.Name)
                {
                    case Field.PanelName.Conveyer_0:
                    case Field.PanelName.Conveyer_90:
                    case Field.PanelName.Conveyer_180:
                    case Field.PanelName.Conveyer_270:
                        AddConveyerNodes(machine.Name, grid);
                        break;

                    case Field.PanelName.Cooker:
                        AddCookerNodes(machine, grid);
                        break;

                    case Field.PanelName.DoubleGate:
                        AddDoubleGateNodes(machine, grid);
                        break;

                    case Field.PanelName.OutputGate:
                        AddOutputGateNodes(machine, grid);
                        break;

                    case Field.PanelName.Picker_0:
                    case Field.PanelName.Picker_90:
                    case Field.PanelName.Picker_180:
                    case Field.PanelName.Picker_270:
                        AddPickerNodes(machine, grid);
                        break;

                    case Field.PanelName.WarpGate_0:
                    case Field.PanelName.WarpGate_90:
                    case Field.PanelName.WarpGate_180:
                    case Field.PanelName.WarpGate_270:
                        AddWarpGateNodes(machine.Name, grid);
                        break;

                    case Field.PanelName.InputGate_Zunda:
                    case Field.PanelName.InputGate_Yudemame:
                    case Field.PanelName.InputGate_Edamame:
                    case Field.PanelName.InputGate_Mochi:
                    case Field.PanelName.InputGate_Rice:
                    case Field.PanelName.InputGate_Kome:
                        AddInputGateNodes(machine, grid);
                        break;
                }
            }
        }
    }

    public void AddConveyerNodes(Field.PanelName name, Vector2 grid)
    {
        Vector2 start;
        Vector2 step;
        Vector2 rowStep;

        switch (name)
        {
            case Field.PanelName.Conveyer_0:
                start = new Vector2(grid.x * LineNodesPerGrid, (grid.y * LineNodesPerGrid) + 1);
                step = Vector2.right;
                rowStep = Vector2.up * 2;
                break;

            case Field.PanelName.Conveyer_90:
                start = new Vector2((grid.x * LineNodesPerGrid) + 1, (grid.y * LineNodesPerGrid) + (LineNodesPerGrid - 1));
                step = Vector2.down;
                rowStep = Vector2.right * 2;
                break;

            case Field.PanelName.Conveyer_180:
                start = new Vector2((grid.x * LineNodesPerGrid) + (LineNodesPerGrid - 1), (grid.y * LineNodesPerGrid) + 1);
                step = Vector2.left;
                rowStep = Vector2.up * 2;
                break;

            default:
            case Field.PanelName.Conveyer_270:
                start = new Vector2((grid.x * LineNodesPerGrid) + 1, grid.y * LineNodesPerGrid);
                step = Vector2.up;
                rowStep = Vector2.right * 2;
                break;
        }

        for (int row = 0; row < 2; ++row)
        {
            var nodeGrid = start + (rowStep * row);
            for (int i = 0; i < (LineNodesPerGrid + 2); ++i)
            {
                if (i < (LineNodesPerGrid + 1))
                {
                    AddNode(nodeGrid, nodeGrid + step);
                }
                else
                {
                    AddNode(nodeGrid, null);
                }
                nodeGrid += step;
            }
        }
    }

    public void AddCookerNodes(Machine machine, Vector2 grid)
    {
        var inputGrid_0 = new Vector2(grid.x * LineNodesPerGrid, (grid.y * LineNodesPerGrid) + 1);
        var inputGrid_1 = inputGrid_0 + (Vector2.up * 2);
        var outputGrid = inputGrid_0 + (Vector2.right * (LineNodesPerGrid - 1));

        AddNode(inputGrid_0, null);
        AddNode(inputGrid_1, null);
        AddNode(outputGrid, outputGrid + Vector2.right);
        AddNode(outputGrid + Vector2.right, outputGrid + (Vector2.right * 2));

        machine.Nodes = new List<Node>() { nodes[inputGrid_0], nodes[inputGrid_1], nodes[outputGrid] };
    }

    public void AddDoubleGateNodes(Machine machine, Vector2 grid)
    {
        AddConveyerNodes(Field.PanelName.Conveyer_0, grid);
        AddConveyerNodes(Field.PanelName.Conveyer_0, grid + Vector2.up);

        var inputGrid_0 = new Vector2((grid.x * LineNodesPerGrid) + 2, (grid.y * LineNodesPerGrid) + 1);
        var inputGrid_1 = inputGrid_0 + (Vector2.up * 2);
        var outputGrid_0 = inputGrid_0 + (Vector2.up * LineNodesPerGrid);
        var outputGrid_1 = outputGrid_0 + (Vector2.up * 2);

        machine.Nodes = new List<Node>() { nodes[inputGrid_0], nodes[inputGrid_1], nodes[outputGrid_0], nodes[outputGrid_1] };
    }

    public void AddOutputGateNodes(Machine machine, Vector2 grid)
    {
        AddConveyerNodes(Field.PanelName.Conveyer_0, grid);

        var output_0 = new Vector2((grid.x * LineNodesPerGrid) + (LineNodesPerGrid - 1), (grid.y * LineNodesPerGrid) + 1);
        var output_1 = output_0 + (Vector2.up * 2);
        machine.Nodes = new List<Node>() { nodes[output_0], nodes[output_1] };
    }

    public void AddPickerNodes(Machine machine, Vector2 grid)
    {
        // !!!
    }

    public void AddWarpGateNodes(Field.PanelName name, Vector2 grid)
    {
        // !!!
    }

    public void AddInputGateNodes(Machine machine, Vector2 grid)
    {
        AddConveyerNodes(Field.PanelName.Conveyer_0, grid);
        var start = new Vector2(grid.x * LineNodesPerGrid, (grid.y * LineNodesPerGrid) + 1);

        machine.Nodes = new List<Node>() { nodes[start] };
    }

    private Node AddNode(Vector2 grid, Vector2? next)
    {
        Node nextNode = null;
        if (next.HasValue)
        {
            if (nodes.ContainsKey(next.Value))
            {
                nextNode = nodes[next.Value];
            }
            else
            {
                nextNode = AddNode(next.Value, null);
            }
        }

        Node node;
        if (nodes.ContainsKey(grid))
        {
            node = nodes[grid];
            if (nextNode != null)
            {
                node.NextNode = nextNode;
            }
        }
        else
        {
            var position = new Vector2((grid.x / LineNodesPerGrid) + (1.0f / LineNodesPerGrid / 2) - 0.5f, (grid.y / LineNodesPerGrid) + (1.0f / LineNodesPerGrid / 2) - 0.5f);
            node = new Node(position, nextNode);
            nodes.Add(grid, node);
        }

        return node;
    }

    private Node AddNode(Node nextNode, Vector2 position)
    {
        var node = new Node(position, nextNode);
        nodes.Add(position / LineNodesPerGrid, node);

        return node;
    }
}
