﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conveyer : MonoBehaviour
{
    private int panelDirection;

    public int Direction
    {
        set
        {
            panelDirection = value;
            transform.rotation = Quaternion.Euler(0, 0, value);
        }
        get
        {
            return panelDirection;
        }
    }
}
