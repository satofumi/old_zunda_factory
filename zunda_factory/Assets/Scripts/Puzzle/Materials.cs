﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Materials : MonoBehaviour
{
    public GameObject TrashPrefab;
    public GameObject ZundaMochiPrefab;
    public GameObject ZundaPrefab;
    public GameObject YudemamePrefab;
    public GameObject EdamamePrefab;
    public GameObject MochiPrefab;
    public GameObject RicePrefab;
    public GameObject KomePrefab;


    public enum Name
    {
        Unknown,

        Trash,

        ZundaMochi,

        Zunda,
        Yudemame,
        Edamame,

        Mochi,
        Rice,
        Kome,
    };


    private Dictionary<Name, GameObject> materialPrefabs;
    private List<Material> allMaterials = new List<Material>();
    private Dictionary<MoveGraph.Node, Material> materialNodes = new Dictionary<MoveGraph.Node, Material>();


    public Material Prefab(Name name)
    {
        if (materialPrefabs == null)
        {
            materialPrefabs = new Dictionary<Name, GameObject>()
            {
                { Name.Trash, TrashPrefab },
                { Name.ZundaMochi, ZundaMochiPrefab },
                { Name.Zunda, ZundaPrefab },
                { Name.Yudemame, YudemamePrefab },
                { Name.Edamame, EdamamePrefab },
                { Name.Mochi, MochiPrefab },
                { Name.Rice, RicePrefab },
                { Name.Kome, KomePrefab },
            };
        }

        var material = Instantiate(materialPrefabs[name]).GetComponent<Material>();
        material.Materials = this;
        material.Name = name;
        allMaterials.Add(material);

        return material;
    }

    public void MoveAllMaterials()
    {
        var removed = new List<Material>();
        foreach (var material in allMaterials)
        {
            material.Move(Time.deltaTime);
            if (material.Removed)
            {
                removed.Add(material);
                Destroy(material.gameObject);
            }
        }

        allMaterials = allMaterials.Except(removed).ToList<Material>();
    }

    public void Remove(Material material)
    {
        materialNodes.Remove(material.Node);
    }

    public void Add(Material material)
    {
        materialNodes.Add(material.Node, material);
    }

    public bool IsEmpty(MoveGraph.Node node)
    {
        return materialNodes.ContainsKey(node) ? false : true;
    }

    public Material NodeMaterial(MoveGraph.Node node)
    {
        return materialNodes.ContainsKey(node) ? materialNodes[node] : null;
    }
}
