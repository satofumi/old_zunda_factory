﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cooker : MonoBehaviour
{
    public GameObject DebugPointPrefab;


    private enum State
    {
        Idle,
        Converting,
    }

    private Machine machine;

    private State state;
    private float elapsed;
    private List<Materials.Name> holdingMaterials = new List<Materials.Name>();
    private Materials.Name convertedMaterialName;

    void Awake()
    {
        machine = GetComponent<Machine>();
        state = State.Idle;
    }

    void Update()
    {
        switch (state)
        {
            case State.Idle:
                for (int i = 0; i < 2; ++i)
                {
                    var material = machine.Materials.NodeMaterial(machine.Nodes[i]);
                    if (material != null)
                    {
                        PickupMaterial(material, machine.Nodes[i].Position);
                    }
                }
                break;

            case State.Converting:
                elapsed += Time.deltaTime;
                if ((elapsed >= Constant.CookerConvertSeconds) && machine.Materials.IsEmpty(machine.Nodes[2]))
                {
                    elapsed -= Constant.CookerConvertSeconds;
                    state = State.Idle;
                    PlaceConvertedMaterial(elapsed);
                    elapsed = 0;
                }
                break;
        }
    }

    public void DrawDebugPoints()
    {
        for (var node = machine.Nodes[2]; node != null; node = node.NextNode)
        {
            var point = Instantiate(DebugPointPrefab, Field.DrawPosition(node.Position), Quaternion.identity);
            point.transform.position -= Vector3.forward * 4;
        }
    }

    private void PickupMaterial(Material material, Vector2 position)
    {
        machine.Materials.Remove(material);
        material.Removed = true;
        holdingMaterials.Add(material.Name);

        if (holdingMaterials.Count >= 2)
        {
            ConvertMaterial();
            holdingMaterials.Clear();
            state = State.Converting;
        }
    }

    private void PlaceConvertedMaterial(float timeDelta)
    {
        var material = machine.Materials.Prefab(convertedMaterialName);
        material.Node = machine.Nodes[2];
        material.Move(timeDelta);
        machine.Materials.Add(material);
    }

    private void ConvertMaterial()
    {
        convertedMaterialName = Convert(holdingMaterials[0], holdingMaterials[1]);
        if (convertedMaterialName == Materials.Name.Unknown)
        {
            convertedMaterialName = Convert(holdingMaterials[1], holdingMaterials[0]);
        }

        if (convertedMaterialName == Materials.Name.Unknown)
        {
            convertedMaterialName = Materials.Name.Trash;
        }
    }

    private Materials.Name Convert(Materials.Name a, Materials.Name b)
    {
        if ((a == Materials.Name.Zunda) && (b == Materials.Name.Mochi))
        {
            return Materials.Name.ZundaMochi;
        }
        else if ((a == Materials.Name.Yudemame) && (b == Materials.Name.Yudemame))
        {
            return Materials.Name.Zunda;
        }
        else if ((a == Materials.Name.Edamame) && (b == Materials.Name.Edamame))
        {
            return Materials.Name.Yudemame;
        }
        else if ((a == Materials.Name.Rice) && (b == Materials.Name.Rice))
        {
            return Materials.Name.Mochi;
        }
        else if ((a == Materials.Name.Kome) && (b == Materials.Name.Kome))
        {
            return Materials.Name.Rice;
        }
        return Materials.Name.Unknown;
    }
}
