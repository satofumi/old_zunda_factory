﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : MonoBehaviour
{
    public Field.PanelName Name { set; get; }
    public List<MoveGraph.Node> Nodes { set; get; }
    public Materials Materials { set; get; }
}
