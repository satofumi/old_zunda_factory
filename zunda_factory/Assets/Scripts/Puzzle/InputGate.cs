﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputGate : MonoBehaviour
{
    public GameObject DebugPointPrefab;


    private Dictionary<Field.PanelName, Materials.Name> panelToMaterials;
    private Materials.Name materialName;
    private Material inputMaterial;

    private Machine machine;
    private float elapsed = Constant.InputGateGenerateInterval;


    void Awake()
    {
        machine = GetComponent<Machine>();
    }

    void Update()
    {
        if (inputMaterial == null)
        {
            inputMaterial = machine.Materials.Prefab(materialName);
            inputMaterial.transform.position = transform.position + Vector3.back;
        }

        elapsed += Time.deltaTime;
        if (elapsed >= Constant.InputGateGenerateInterval)
        {
            elapsed -= Constant.InputGateGenerateInterval;
            if (machine.Materials.IsEmpty(machine.Nodes[0]))
            {
                GenerateMaterial(elapsed);
            }
        }
    }

    private void GenerateMaterial(float timeDelta)
    {
        var material = machine.Materials.Prefab(materialName);
        material.Node = machine.Nodes[0];
        material.Move(timeDelta);
        machine.Materials.Add(material);
    }

    public void DrawDebugPoints()
    {
        for (var node = machine.Nodes[0]; node != null; node = node.NextNode)
        {
            var point = Instantiate(DebugPointPrefab, Field.DrawPosition(node.Position), Quaternion.identity);
            point.transform.position -= Vector3.forward * 4;
        }
    }

    public void SetInputMaterial(Field.PanelName name)
    {
        if (panelToMaterials == null)
        {
            panelToMaterials = new Dictionary<Field.PanelName, Materials.Name>()
            {
                { Field.PanelName.InputGate_Zunda, Materials.Name.Zunda },
                { Field.PanelName.InputGate_Yudemame, Materials.Name.Yudemame },
                { Field.PanelName.InputGate_Edamame, Materials.Name.Edamame },
                { Field.PanelName.InputGate_Mochi, Materials.Name.Mochi },
                { Field.PanelName.InputGate_Rice, Materials.Name.Rice },
                { Field.PanelName.InputGate_Kome, Materials.Name.Kome },
            };
        }

        materialName = panelToMaterials[name];
    }
}
