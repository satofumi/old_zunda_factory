﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleGate : MonoBehaviour
{
    private Machine machine;
    private Material[] copiedMaterials = new Material[2];

    void Awake()
    {
        machine = GetComponent<Machine>();
    }

    void Update()
    {
        for (int i = 0; i < 2; ++i)
        {
            var material = machine.Materials.NodeMaterial(machine.Nodes[i]);
            if (material == null)
            {
                copiedMaterials[i] = null;
            }
            else
            {
                if (copiedMaterials[i] != material)
                {
                    copiedMaterials[i] = material;
                    if (machine.Materials.IsEmpty(machine.Nodes[2 + i]))
                    {
                        CopyMaterial(material.Name, machine.Nodes[2 + i]);
                    }
                }
            }
        }
    }

    private void CopyMaterial(Materials.Name materialName, MoveGraph.Node node)
    {
        var material = machine.Materials.Prefab(materialName);
        material.Node = node;
        material.Move(0);
        machine.Materials.Add(material);
    }
}
