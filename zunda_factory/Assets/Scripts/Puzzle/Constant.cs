﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constant
{
    public const int ZpsCalculatePerios = 8;
    public const float InputGateGenerateInterval = 0.75f;
    public const float MaterialMoveSecondsPerGrid = 0.15f;
    public const float CookerConvertSeconds = 0.20f;

}
