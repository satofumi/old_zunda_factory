﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Material : MonoBehaviour
{
    public Materials Materials { set; get; }
    public Materials.Name Name { set; get; }
    public MoveGraph.Node Node { set; get; }
    public bool Removed { set; get; }


    private float elapsed;


    public void Move(float timeDelta)
    {
        if (Node == null)
        {
            return;
        }

        elapsed += timeDelta;
        if (elapsed >= Constant.MaterialMoveSecondsPerGrid)
        {
            if (!Materials.IsEmpty(Node.NextNode))
            {
                // 移動先に材料がある場合、現在の位置を保持する。
                elapsed -= timeDelta;
                return;
            }

            elapsed -= Constant.MaterialMoveSecondsPerGrid;
            Materials.Remove(this);
            Node = Node.NextNode;

            if ((Node == null) || (Node.NextNode == null))
            {
                // ゴミ化するアニメーションを表示する。
                // !!!
                Removed = true;
                return;
            }

            Materials.Add(this);
        }

        var currentPosition = Vector2.Lerp(Node.Position, Node.NextNode.Position, elapsed / Constant.MaterialMoveSecondsPerGrid);
        transform.position = Field.DrawPosition(currentPosition) + Vector3.back;

        return;
    }
}
