﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectController : MonoBehaviour
{
    public Text VersionText;
    public Text ZundaCountText;


    private int zundaCount { set; get; }

    void Start()
    {
        VersionText.text = "Version: " + Application.version;
    }

    void Update()
    {
        // !!! sum factories zps

        UpdateZundaCountText();
    }

    private void UpdateZundaCountText()
    {
        var unit = (zundaCount > 1) ? "zundas" : "zunda";
        ZundaCountText.text = $"{zundaCount} <size=20>{unit}</size>";
    }

    public void ZundaButtonPressed()
    {
        ++zundaCount;
    }
}
